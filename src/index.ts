// ---------------- 基本類型 ----------------
let str: string = 'Jane';
let num: number = 1000;
let boo: boolean = true;
let n: null = null;
let un: undefined = undefined;

let test: any = 'Jane'; // 用這種類型等於寫 js (什麼類型都可以)
// 真正開發時這樣寫就可以判斷是什麼類型
let title = 'Jane';

// 陣列
let arr1: string[] = ['a', 'b'];
let arr2: string[][] = [['aa', 'bb'], ['1']];
let arr3: Array<string> = [] // 泛型宣告 arr1/arr3宣告方式一樣
let arr4: (string | boolean)[] = [];


// 元組
let tuple1: [number, string, boolean] = [1, 'a', true];
let tuple2: [number, number][] = [[11, 22], [33, 44]];

// 物件
let newObj: { name: string, age: number };
newObj = {
    name: 'Jane',
    age: 25
}

// ---------------- enum ----------------
enum liveStatus {
    SUCCESS = 0,
    FAIL = -1,
    STREAMING = 1
}

const result = liveStatus.SUCCESS;
console.log(result)

// ---------------- union ----------------
let aaa: (boolean | string)[] = [];
aaa.push(false);
aaa.push('');

let bbb: number | string;
bbb = 'str'
bbb = 1000
// aaa=true error,不可boolean

// ---------------- never 類型 ----------------
// 永遠不可能發生
// if(typeof aaa === 'string'){
//     aaa.split();
// }

// ---------------- 強制斷言 ----------------
let aaa1 = 999
let aaa2 = aaa1 as unknown as string // 先轉成 unknown 再轉成 string
// unknown 是比較安全的 any

// ---------------- type ----------------
// 定義一個 LiveName 類型
type LiveName = string | boolean | number
//  name1 / name2 為 LiveName 類型
let name1: LiveName
let name2: LiveName

type A = number | string // 自定義類型
type B = boolean | string

let a1: A
a1 = 999
a1 = 'str'
// a1=true  //error because boolean isn't assignable to type A.

let b1: B
b1 = true

// ---------------- interface ----------------
interface User {
    name: string;
    age: number;
}

// ---------------- object ----------------
type Card = { // 自定義 Card 類型
    name: string
    desc: string,
}
const obj: Card = {  // 宣告 obj 為 Card 型態
    name: 'Jane',
    desc: 'girl'
}

interface Card2 { // interface 內容可擴充
    name: string
    desc: string,
}
interface Card2 { // interface 內容可擴充
    age?: number // 加問號，可number可undefined
    // age:number 只能number
}
const obj2: Card2 = {
    name: 'Jane',
    desc: 'girl',
    age: 18
}

// ---------------- function ----------------
// 參數
function hello(a: string, b: string) {
    return a + b;
}
function hello2(a: string, b: string): number {
    console.log(a, b)
    return 999;
}
function hello3(a: number, b: boolean, c: string) {
    return 100;
}

// undefined
function hello4(name: string, age?: number) { // 問號的參數放後面
    let a: number
    // a=age 會報錯，age可能傳undefined，因為宣告age?

    if (age === undefined) return -1; // 寫個判斷式，避免 test2()收到undefined
    test2(age)
}
function test2(a: number) {
    console.log(a)
}

const func = () => {

}
